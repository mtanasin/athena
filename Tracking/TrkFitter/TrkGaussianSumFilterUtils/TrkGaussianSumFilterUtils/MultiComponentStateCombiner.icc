/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file   MultiComponentStateCombiner.icc
 * @author Atkinson,Anthony Morley, Christos Anastopoulos
 *
 * Implementation code for MultiComponentStateCombiner
 */

#include "CxxUtils/phihelper.h"
namespace Trk {

namespace MultiComponentStateCombiner {
inline void
combineParametersWithWeight(AmgVector(5) & firstParameters,
                            double& firstWeight,
                            const AmgVector(5) & secondParameters,
                            const double secondWeight)
{

  double totalWeight = firstWeight + secondWeight;
  // Ensure that we don't have any problems with the cyclical nature of phi
  // Use first state as reference poin
  double deltaPhi = firstParameters[2] - secondParameters[2];
  if (deltaPhi > M_PI) {
    firstParameters[2] -= 2 * M_PI;
  } else if (deltaPhi < -M_PI) {
    firstParameters[2] += 2 * M_PI;
  }
  firstParameters =
    firstWeight * firstParameters + secondWeight * secondParameters;
  firstParameters /= totalWeight;
  // Ensure that phi is between -pi and pi
  firstParameters[2] = CxxUtils::wrapToPi(firstParameters[2]);
  firstWeight = totalWeight;
}

inline void
combineCovWithWeight(const AmgVector(5) & firstParameters,
                     AmgSymMatrix(5) & firstMeasuredCov,
                     const double firstWeight,
                     const AmgVector(5) & secondParameters,
                     const AmgSymMatrix(5) & secondMeasuredCov,
                     const double secondWeight)
{
  double totalWeight = firstWeight + secondWeight;
  AmgVector(5) parameterDifference = firstParameters - secondParameters;
  parameterDifference[2] = CxxUtils::wrapToPi(parameterDifference[2]);
  parameterDifference /= totalWeight;
  firstMeasuredCov *= firstWeight;
  firstMeasuredCov += secondWeight * secondMeasuredCov;
  firstMeasuredCov /= totalWeight;
  firstMeasuredCov += firstWeight * secondWeight * parameterDifference *
                      parameterDifference.transpose();
}

}

}
